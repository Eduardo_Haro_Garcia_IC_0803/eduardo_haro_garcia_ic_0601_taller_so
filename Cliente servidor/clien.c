#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <netinet/tcp.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>

#define TAM 300
#define TAM2 925

int main(int argc, char *argv[]){
  int PORT;
  char *IP;
  system("clear");
  if(argc < 3){
    printf("ingresa la IP: \n\n");
    char portc[4];
    gets(portc);
    PORT = atoi(portc);
    printf("\t\tIngresa el puerto: ");
    gets(IP);
  }else{
    IP = argv[1];
    PORT = atoi(argv[2]);
  }

  int FD, nBytes;
  char FD2[TAM];

  struct hostent *HOST; //info por parte del host
  struct sockaddr_in SERVER; //dir host

  if((HOST=gethostbyname(IP)) == NULL){
    perror("error.");
    return -1;
  }
  if((FD = socket(AF_INET, SOCK_STREAM, 0)) == -1){
    perror("No se puede crear socket");
    return -1;
  }
  SERVER.sin_family = AF_INET;
  SERVER.sin_port = htons(PORT);
  SERVER.sin_addr = *((struct in_addr *)HOST->h_addr);
  bzero(&(SERVER.sin_zero),8);


  if(connect(FD, (struct sockaddr *)&SERVER, sizeof(struct sockaddr)) == -1){
    perror("No se puede conectar con el servidor");
    return -1;
  }
  //LLAMAMOS A RECV 
  nBytes = recv(FD,FD2,TAM2,0);
  FD2[nBytes] = '\0';
  printf("Mensaje: %s\n",FD2);
  char num1[TAM], num2[TAM], *num = (char *)malloc(TAM);
  printf("Primer num:");
  scanf("%s",num);
  strcpy(num1,num);
  send(FD,num1,TAM2,0);
  nBytes = recv(FD,FD2,TAM2,0);
  FD2[nBytes] = '\0';
  printf("Recibio: %s\n",FD2);
  printf("Num dos enviado");
  scanf("%s",num);
  strcpy(num2,num);
  send(FD,num2,TAM2,0);
  nBytes = recv(FD,FD2,TAM2,0);
  FD2[nBytes] = '\0';
  printf("S--->Recibio: %s\n",FD2);
  nBytes = recv(FD,FD2,TAM2,0);
  FD2[nBytes] = '\0';
  printf("\nS---> Resultado de suma%s\n",FD2);
  close(FD);
}
